
* [http://www.linuxproblem.org/art_9.html][http://www.linuxproblem.org/art_9.html](SSH login without password)

* [How to install KVM on Ubuntu](https://www.linuxtechi.com/install-configure-kvm-ubuntu-18-04-server/) - In this article we will discuss how to install and configure KVM hypervisor on Ubuntu 18.04 LTS server. Good foundation to Install KVM support directly on ubuntu.

* [enable ssh on ubuntu](http://ubuntuhandbook.org/index.php/2014/09/enable-ssh-in-ubuntu-14-10-server-desktop/)


* [How do I create a EFI-bootable ISO of customized version of Ubuntu](https://askubuntu.com/questions/457528/how-do-i-create-an-efi-bootable-iso-of-a-customized-version-of-ubuntu)  We need this so we can auto create the security and disk configuration without having to manually plug a USB into every machine.

* [jpw do I create completely unattended ubuntu install](https://askubuntu.com/questions/122505/how-do-i-create-a-completely-unattended-install-of-ubuntu) - Includes a Kickstart script and how to apply it.   [Shell script creating CD for unattended Ubuntu Server installations](http://www.utech.de/2013/05/shell-script-creating-a-cd-for-unattended-ubuntu-server-installations)  [How do I create a completely unattended install for Ubuntu](http://www.eonlinegratis.com:80/2013/how-do-i-create-completely-unattended-install-for-ubuntu/)

* [How to install real ubuntu on USB flash drive](http://ubuntuhandbook.org/index.php/2014/11/install-real-ubuntu-os-usb-drive/)



* [Cloud Init Docs](https://cloudinit.readthedocs.io/en/latest/) is the defacto multi-distribution package that handles early initialization of a cloud instance.

* [ How to use cloud init to configure your server at first boot](https://www.scaleway.com/docs/how-to-use-cloud-init-to-configure-your-server-at-first-boot/)

* [Cloud init 18.5 users guide PDF](https://media.readthedocs.org/pdf/cloudinit/stable/cloudinit.pdf)

* [How can I add an additional SSH user account with cloud-init and user data for my EC2 instance?](https://aws.amazon.com/premiumsupport/knowledge-center/ec2-user-account-cloud-init-user-data/)

* [How To Use Cloud-Config For Your Initial Server Setup ](https://www.digitalocean.com/community/tutorials/how-to-use-cloud-config-for-your-initial-server-setup)

* [Core Cloud init docs](https://cloudinit.readthedocs.io/en/latest/index.html)


* [Cloud init examples] (https://cloudinit.readthedocs.io/en/latest/topics/examples.html) Shows how to add SSH users,  Install apt-get packages,  Run commands on first boot,  Adding trusted CA certificates,  Writing out Arbitrary files, 

* [Running LXD commands at cloud init start](https://www.cyberciti.biz/faq/run-commands-on-linux-instance-at-launch-using-cloud-init/)   shows how to pass cloud init into LXC config before image start. 

* [LXD read the docs container configuration](https://lxd.readthedocs.io/en/latest/containers/)


* [Different files to update when saving enviornment variables in Linux](https://www.tecmint.com/set-unset-environment-variables-in-linux/)

* [Setup a ZFS pool for LXC containers](https://angristan.xyz/lxc-zfs-pool-lxd/) ZFS is an awesome file system. It's a 128 bits file system meaning that we can store a nearly unlimited amount of data (no one will never attain its limit). It replaces RAID arrays by much simpler, safer and faster "pools", and had very good performance by using compression, copy-on-write, dynamic block size, dynamic stripping, and an extensive use of RAM cache. 

** [ZFS zpool explained](https://wiki.ubuntu.com/Kernel/Reference/ZFS?_ga=2.76664790.216778258.1545007173-666935123.1542481114) - clone,  dedub, raid,  stripe across drives, mirroring,  transparant support for caching drives,   Snapshots,   Clones,   Send Snapshot remote,  Ditto blocks to create additional duplicates, fast compression on write.

* [Noob's Guide to Gluster on ZFS](https://medium.com/@glmdev/a-noobs-guide-to-gluster-on-zfs-4f0ad156970f) includes setting up SAMBA shares in Linux,   Instructions on how to create new ZFS pools, 


* [How to create a bootable USB disk for Ubuntu](https://www.linuxtechi.com/create-bootable-usb-disk-dvd-ubuntu-linux-mint/)

* [ssh essentials](https://www.digitalocean.com/community/tutorials/ssh-essentials-working-with-ssh-servers-clients-and-keys) explanation of how to do most of things we need with SSH including making public keys easily available to make provisioning remote servers easy.   Forwarding ports,  Keeping connections alive,  Changing Listen Port,  Forwarding X terms,  Service restart,  Allow access by group membership, Disabling password authentication [How to setup SSH keys on ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-1804)  [open ssh server docs](https://help.ubuntu.com/lts/serverguide/openssh-server.html.en)


* [Awk by Example](https://www.shortcutfoo.com/blog/awk-by-example/)
* [Editing Awk files in place with backup using gawk](https://stackoverflow.com/questions/16529716/awk-save-modifications-in-place)  gawk -i inplace -v INPLACE_SUFFIX=.bak '{print $1}' file

* [35 examples of RegEx patterns in AWK](https://dzone.com/articles/35-examples-of-regex-patterns-using-sed-and-awk-in) - 
