#!/bin/bash
# scripts/host_prep/gigabyte_brix_setup.sh
# Scripts needed to setup the Wifi configuration on the brix. I needed this because
# I wanted to run them ontop of a bookcase and didn't want to run network cables up to them.
# I used my router function to map the MAC address from the WIFI interface in the 
# BRIX to a known IP address.
# 
accessPointName=$1 
wepPassword=$2 
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

echo "accessPointName: $accessPointName  wepPassword: $wepPassword"

if [ "$#" -ne "2" ]; then
  msg='ABORT: gigabyte-brix-setup.sh invalid number of aurguments
  
  $1 - Name of WiFi access point to connect to
  
  $2 - WEP password to used to connect to WIFI router. 
         
  returns 0 response code if sucess otherwise 1
       
  Example: 
    bash gigabyte-brix-setup.sh  trendnetx12 ABC123321
  '
  # Send msg to stderr
  1>&2 echo "$msg"
  exit 1
fi


#TODO: Try to Ping Yahoo.com and abort if not sucess
# with message indicating they need to fix their 
# DNS configuration.  
# https://www.techrepublic.com/article/how-to-set-dns-nameservers-in-ubuntu-server-18-04/


# Install the wireless tools and network manager
sudo apt-get install wireless-tools network-manager -y
# TODO: Check Status and abort if not success

# Connect to the Acess point using the user supplied WEB password
sudo nmcli d wifi connect $accessPointName password $wepPassword
# TODO: Check Status and abort if not success

echo "MAC address of the Access point if sucessful connect"
echo $(nmcli -f BSSID,ACTIVE dev wifi list | awk '$2 ~ /yes/ {print $1}')

echo "MAC address of the local PC"
echo $(nmcli | grep wifi)

echo "IP address of local PC WiFi"
echo $(nmcli | grep inet4)

# Install other pre-requisites 
bash $scdir/basic_ubuntu_18_host_setup.sh

