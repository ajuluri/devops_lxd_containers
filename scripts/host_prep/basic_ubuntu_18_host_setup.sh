#!/bin/bash
# scripts/basic_ubuntu_18_host_setup.sh
#
# Script to install the basic set of packages we are using on our LXD control PC
# Not all of these would be needed on all hosts.
# One thing you will need to know before hand is the block device you want LXD 
# to use for storage unless you plan to use the dir option during initialization.
#
scriptName="$0"
# Directory where this script exists
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
scriptBase="$scdir/.."

echo "$0  USER: $USER"

#Update local packages
sudo apt update
sudo apt-get dist-upgrade -y

# Utilities we commonly use.
sudo apt-get install acl cgroup-bin unzip zip  python git-core  jq network-manager -y

#install lxc to get all commands
#otherwise lxc-attach is missing
#and is needed for some configuration commands.
#sudo apt-get install lxc -y

#Install lxd
sudo apt install lxd -y
# I choose the option for 3.0 first and then 
# removed it and chose lattest features. 


# Add your user to lxd group otherwise lxc commands
# will fail due to socket permission errors.
sudo adduser $USER lxd

# Install Tools needed to support the Go Language utilities
#sudo apt-get install golang  -y

# Install some of our commonly used tools
#############
## Not Strickly needed but kept here just to make life 
## easier for when I do need them.
#############
# clone the devops lxd containers project 
# to local drive into the devops directory.
sudo apt-get install golang p7zip-full zsh xzip tar rsync pxz xz-utils python-lzma libarchive-tools zsh shellcheck openssl golang ecryptfs-utils atop nmon  zfsutils-linux -y

git clone https://bitbucket.org/joexdobs/devops_lxd_containers ~/devops

git clone https://bitbucket.org/joexdobs/computer-aided-call-response-engine ~/cacre

git clone https://bitbucket.org/joexdobs/meta-data-server ~/mds 

sudo chown -R $USER ~/cacre
sudo chown -R $USER ~/devops
sudo chown -R $USER ~/mds


# Cleanup and remove old packages
sudo apt autoremove
