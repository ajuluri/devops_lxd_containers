#!/bin/bash
# Add the index.txt as valid type when accessing directories

export cwd=$(pwd)
# Make a local backup
echo "make backup to dir.conf.bak"
cp /etc/apache2/mods-enabled/dir.conf dir.conf.bak
# Modify the conf file
echo "attempt text expansion"
sed -E "s/DirectoryIndex\s+index.html/DirectoryIndex index.txt index.html/g" /etc/apache2/mods-enabled/dir.conf  > dir.conf.mod
# Overlay the exisitng file with new one.
echo "copy dir.conf.mod to /etc/apache2/mods-enabled/dir.conf"
cp dir.conf.mod /etc/apache2/mods-enabled/dir.conf
 
