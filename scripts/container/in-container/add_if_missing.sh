#!/bin/bash
# Search for parameter $1 and if not found then add parameter $2 to file.
search=$1
repstr=$2
fname=$3
echo "first arg: $search"
echo "second arg: $repstr"
echo "fname: $fname"
grep -q -F "$search" $fname || echo "$repstr" >> $fname
tail $fname
# TODO: Replace line that defined variable if it exists
# TODO: Add segment to PATH if does not exist
