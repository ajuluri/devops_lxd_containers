#!/bin/bash
# scripts/get_current_primary_ip.sh

# This one returns multiple addresses
# (ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')

 (hostname -I | cut -d' ' -f1)
