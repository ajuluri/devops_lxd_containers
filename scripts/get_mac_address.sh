#!/bin/bash
# Returns local MAC address for default Ethernet
# Requires sudo apt  install network-manager 
# WARNING: Ethernet device naming is complex and has considerable variability
#  This script may need to be adjusted to reflect local server device names.
#  https://people.freedesktop.org/~lkundrak/nm-docs/nmcli-examples.html
#  https://developer.gnome.org/NetworkManager/stable/nmcli.html
# replace enp3s0 with your local device

echo $( nmcli -t -f general, device show enp3s0 | grep GENERAL.HWADDR |  grep -o -e "..:..:..:..:..:..")