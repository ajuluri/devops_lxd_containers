#!/bin/bash
# Produce a fully hardened haproxy image from hardenedUbuntu image..

# Save script Dir so we can make sub script calls relative
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
imgName=wrkHAProxy
publishImg=basicHAProxy
template=hardenedUbuntu
templateCreateScript=$scdir/produce_updatedUbuntu.sh

source $scdir/produce_image_setup.sh
##############
## START ACTUAL HAPROXY WORK HERE
##############
echo "Install the haproxy package"
lxc exec $imgName -- apt install -y haproxy

# Show the version of haproxy installed
lxc exec $imgName -- haproxy -v

# socat is needed to support the haproxy
# dynamic configuration API
echo "install socat to support dynamic API"
lxc exec $imgName --  apt install socat

echo "display contents of the current haproxy.cfg file"
lxc exec $imgName -- cat /etc/haproxy/haproxy.cfg

# Start haproxy with the default configuration file.
lxc exec $imgName --  haproxy -f /etc/haproxy/haproxy.cfg       -D -p /var/run/haproxy.pid -sf $(cat /var/run/haproxy.pid)

#####
## TODO: Add Layer 7 Proxy Configuration But need to have it specified install
##   From devops-with-lxd.md file
## previously generated Proxy configuration files. 
######

bash $scdir/publish_image_and_cleanup.sh $imgName $publishImg

