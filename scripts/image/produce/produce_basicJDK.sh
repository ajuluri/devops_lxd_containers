#!/bin/bash
# Produce a basic JDK image from hardenedUbuntu image..

scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
imgName=wrkJDK
publishImg=basicJDK
template=hardenedUbuntu
templateCreateScript=produce_hardenedUbuntu.sh

source $scdir/produce_image_setup.sh


##############
## START ACTUAL Tomcat WORK HERE
##############

# Install the deafult JDK
lxc exec $imgName -- apt-get install default-jdk --assume-yes

# TODO: Add Hardening HERE

bash $scdir/publish_image_and_cleanup.sh $imgName $publishImg

