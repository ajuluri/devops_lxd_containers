#!/bin/bash
#scripts/image/produce/apacheKV.sh
#  Takes a named directory containing values produced with 
#  file2consul and transfers the file to apache server 
#  to replace content in /var/www/html so it shows
#  up in apache as /kv for the root of the config tree.
#  The file to consule tool creates one file for each
#  unique KV entry in the configuration tree.  It walks
#  the key space creating a directory for each / in the 
#  key space.  For each unique directory it also creates 
#  a index.txt file listing all the keys in the file. 
#
#  The resulting directory will be created in /var/www/html 
#  with the same name of the directory you created the 
#  zip from. By convention this should have been kv
#
# This is based on the premis that we do not really need
# realtime changes to an existing kv config store. If we
# build it as part of an image we can run multiple of them
# and get HA and scalability. 
# 
# name directory

imageSuffix=$1
kvSrcDir=$2
imgName=kvWrkApache
basePublishImg=apacheKV
template=basicApache
templateCreateScript=produce_basicApache.sh
# Save script Dir so we can make sub script calls relative
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
scriptName="$0"
# scriptBase is shorthand for our location relative
# to the /scripts directory so we can just specify 
# the path of other scripts relative to it.
scriptBase="$scdir/../.."
exampleBase="$scriptBase/../example"
# Load our Utility utility_functions
# Image Suffix is added to the apacheKV to make it unique for the kv files being 
# loaded.  If not specified will set to "" 

# Name of Image we will actualy produce. 
publishImg=$basePublishImg$imageSuffix

# Produce a hopefully unique image name that will not 
# collide with any other image names already running
# in same host. 
imgName=wrk$imgName$imageSuffix

# Load common Utility function along with some
# pre-definits.  This file depends on $scriptName
# $scdir $scriptBase, $exampleBase being set before
# it is ran. 
source $scriptBase/utility_functions.sh

echo "cwd: $cwd  scdir=$scdir imageSuffix:$imageSuffix  srcFi: $srcFi publishImg: $publishImg"


# Print help message Exit if no parameters
if [ "$#" -ne "2" ]; then
  msg='ABORT: apacheKV.sh  invalid number of aurguments
  
  $1 - Immage suffix appended to our base image 
       name to produce a new image uniquely named
       to indicate what KV set it has been loaded with
  
  $2 - Name of file Directory that contains the KV
       directory structure and files that will be loaded
       into apache.  This file is normally generated 
       using our file2consul utility with the opiton
       to create a directory tree. 
       
  returns 0 response code if sucess otherwise 1
       
  Example: 
    bash apacheKV.sh kvsample ../../../example/kv-sample/kv
  '
  # Send msg to stderr
  1>&2 echo "$msg"
  exit 1
fi



if [ -d $srcDir ]; then
   echo "srcFi $srcDir  exists "
else
   echo "ABORT: $srcDir does not exist"
   exit 1
fi


# Boot our template image into our computed work image Name
# and delete any images running with the smae name. 
source $scdir/produce_image_setup.sh


################
### Start Actual customization work
################
contWebDir=/var/www/html
contWebPushDir=$imgName$contWebDir
contWebPushDir=$(removeDuplicateSlash $contWebPushDir)

# The apache default web content goes in /var/www/html
# so we need to copy our content there to replace the 
# existing content.

# Add zip so we can unzip our file bundles
# No longer need zip because changed to just
# recursively pushing the files.
#lxc exec $imgName -- apt install unzip
#lxc exec $imgName -- apt install libarchive-tools --assume-yes


echo "lxc file push $kvSrcDir $imgName/var/www/html/"
lxc file push --recursive $kvSrcDir $contWebPushDir

lxc exec $imgName -- rm /var/www/html/index.html
#  Copy the index.txt from our kvsample to to replace the
#  index.html. 
lxc file push $kvSrcDir/../index.txt $imgName/$contWebDir/index.txt

# NOTE: Run with bash -x to see exactly how this is
# getting executed. with all expansions in place

# Modify the file /etc2/apache2/mods-available/dir.conf
# modify file /etc/apache2/mods-enabled/dir.conf
# To include index.txt
hostLocinContScripts=$scdir/../../container/in-container
contTargetScripts=/root/scripts
contLocScripts=/root/scripts/in-container
contScriptName=$contLocScripts/apache/add_txt_ext_to_index_for_dir.sh
contScriptPushLoc=$(removeDuplicateSlash "$imgName/$contTargetScripts")


# Copy our in-container scripts over to the hostLocinContScripts
# into /root/scripts and then run the script to add index.txt 
# to the directory indexing file.
lxc exec $imgName -- mkdir -p $contTargetScripts
lxc exec $imgName -- chmod 773 /root/scripts
lxc file push --recursive $hostLocinContScripts $contScriptPushLoc
lxc exec $imgName -- chmod 773 $contScriptName
lxc exec $imgName -- bash $contScriptName
echo "try to show changes to /etc/apache2/mods-enabled/dir.conf"
#lxc exec $imgName -- cat /etc/apache2/mods-enabled/dir.conf

   
# Change Listen Port from port 80 to better reflect ports where
# popular config stores listen


# Restart apache so it picks up the config changes
lxc exec $imgName -- systemctl restart apache2.service
# Give service time to restart
sleep 5

# Test to see if we can access the content
#curl 127.0.0.1/kv-sample/build/blog/max_post_age_days
echo "try to read kv at /kv/build/blog/remote_server"
cres1=$(curl -sb -H $containerIP/kv/build/blog/remote_server)
echo "curl res: $cres1"



echo "try to read kv at directory /kv/build/blog/"
cres2=$(curl -sb -H $containerIP/kv/build/blog/)
echo "curl res: $cres2"



bash $scdir/publish_image_and_cleanup.sh $imgName $publishImg
