# install_zookeeper.sh
# Install and start Zookeeper 
# Isolated here to make it easier to 
# test in isolation
# https://zookeeper.apache.org/doc/r3.3.3/zookeeperAdmin.html#sc_strengthsAndLimitations
# https://zookeeper.apache.org/doc/r3.3.3/zookeeperStarted.html

#
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )

groupadd zookeeper
useradd -m -d /home/zookeeper -g zookeeper -G sudo zookeeper

# Save Basic zookeeper-conf 
mkdir -p /etc/zookeeper/conf  
echo '
tickTime=2000
dataDir=/home/zookeeper/data
clientPort=2181
maxClientCnxns=0 ' > /etc/zookeeper/conf/zoo.cfg



apt-get install net-tools -y
apt-get install zookeeperd -y

##########
### TODO: Customize Zookeeper Setup
##########

chown -R zookeeper:zookeeper /home/zookeeper
chown -R zookeeper:zookeeper /var/lib/zookeeper
chown -R zookeeper:zookeeper /etc/zookeeper



########
### TODO: Customize Zookeeper to know about
###  3 other zookeeper with passed in server
###  lists
########
 # Zookeeper.conf needs at least 3 servers to run in durable
 # mode.  We will need to pass that list of servers into our
 # config and all of them need to be up before the system will 
 # work. 
### adduser: Warning: The home directory `/var/lib/zookeeper' does not belong to the user you are currently creating.
### update-alternatives: using /etc/zookeeper/conf_example to provide /etc/zookeeper/conf (zookeeper-conf) 
### ls /etc/zookeeper/conf 

### TODO:  Need to edit this file to include at least 3 servers
###  to host zookeeper.
### cat /etc/zookeeper/conf/zoo.cfg





###########
### Start Zookeeper
###########

##systemctl status zookeeper
##echo "zookeeper status"
## NOTE: Kafka includes script to start zookeeper
##  So not using these.
#sleep 5
#echo "Start Zookeeper"
#systemctl start zookeeper
#sleep 5
#echo "zookeeper started"
#systemctl enable zookeeper
#echo "zookeeper enabled" 
#sleep 5
#echo "Show whether zookeeper is running"
#netstat -tulpen | grep 2181
#echo "L61: Zookeper setup"

echo "Lock zookeeper user to prevent login"
sudo passwd zookeeper -l
sudo deluser zookeeper sudo
echo "$0 successful exit"
exit 0