#!/bin/bash
# post-boot-init.kafka.sh
#
# Perform post boot initialization specific to each Kakfa Instance
# that change between instances at runtime.
#
# We built our Kakfa image to be generic across all nodes in the fleet.
# We need to adjust our configuration files to reflect what ENV it is 
# running in,  What the unique Node ID is for both zookeeper and kafka
# Where to send it's logs, etc. 
#
# The System Init will run this file on startup.  We will also change CRON
# to run it every 10 minutes. 
#
#
