#!/bin/bash
# on-launch.sh
# Ran when the container is launched after all the enviornment variables are set by the launch script.  May need to 
# re-write portions of the zookeeper and kafka config file properly use the passed in parameters.

# See: https://kafka.apache.org/documentation/#brokerconfigs
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
function timeMS() {
 echo $(date +%s%N | cut -b1-13)
}

echo "Environment variables containing ENV"
set | grep ENV
echo "Environment variables containing kafka"
set | grep kafka
currMS=$(timeMS)

# TODO: Check the string we just built and see if anything interesting has changed
# and only run the portions below that apply.
numHost=0
for EACH in `echo "$kafkaHostList" | grep -o -e "[^,]*"`; do
    let numHost=numHost+1        
done
echo "found $numHosts hosts"


# Build or save list of enviornment variables
envvar="
ENV=$ENV
kafkaHostList=\"$kafkaHostList\"
kafkaHostId=$kafkaHostId
kafkaHostName=$kafkaHostName
kafkaHostIP=$kafkaHostIP
kv=$KV
logDest=$logDest
"
# Save Enviornment variables passed in to the /etc/enviornment so they will
# be available after next boot or restart.
echo -e "$envvar" >> /etc/environment
echo -e "$envvar" >> /home/kafka/env-set.txt


# Make a backup of our starting point for Kafka Config and File to use for wrk..
 cp  /home/kafka/kafka/config/server.properties /home/kafka/kafka/config/server.properties.$currMS.bak
 cp  /home/kafka/kafka/config/server.properties /home/kafka/kafka/config/server.properties.wrk
 
 wrkFi=/home/kafka/kafka/config/server.properties.wrk
# Modify Kafka Config so it has the right Broker.ID
gawk -i inplace '{sub(/broker.id=[0-9]+/, "broker.id=$kafakInstanceId"); print}' $wrkFi
  
#Modify Kafka so it logs / Message Traffic to /home/kafka/log
gawk -i inplace '{sub(/log.dirs=.+$/, "log.dirs=/home/kafka/log"); print}'  $wrkFi
 
 #Modify the Replication Factor to at least 3
 gawk -i inplace '{sub(/offsets.topic.replication.factor=[0-9]+/, "offsets.topic.replication.factor=3"); print}' $wrkFi

# Modify Kafka Config so it reflects this Hosts IP / Name 
# host.name=ec2-<IP1>.amazonaws.com #for 2nd EC2 instance it'll be "ec2-<IP2>.amazonaws.com"

# Modify Zookeepr config and Kafka server.properites for total number of servers.

 gawk -i inplace '{sub(/num.partitions=[0-9]+/, "num.partitions=$numHost"); print}' $wrkFi
#num.partitions=4



echo "auto.create.topics.enable=true" >> $wrkFi



cp $wrkFi  /home/kafka/kafka/config/server.properties 


# Save Basic zookeeper-conf  but update it to include 
# the server configuration data
mkdir -p /etc/zookeeper/conf  
echo -e '
tickTime=2000
dataDir=/home/zookeeper/data
clientPort=2181
maxClientCnxns=0 ' > /home/kafka/kafka/config/zoo.cfg

#https://gist.github.com/mkanchwala/fbfdd5ef866a58a77f6e
#https://kafka.apache.org/documentation/#brokerconfigs - See the section on  SSL Keys 

echo -e "
initLimit=5
syncLimit=2
" > /home/kafka/kafka/config/zookeeper.properties

# We have to list each Server in the Cluster for Zookeeper 
for EACH in `echo "$kafkaHostList" | grep -o -e "[^,]*"`; do
    echo "Found: \"$EACH\""
    let hostId=hostId+1    
    lxdHost=$(echo $EACH | cut -d '=' -f 1)
    hostIP=$(echo $EACH | cut -d '=' -f 2)    
    
    kafkaContName="$lxdHostKafka$hostId"
    echo "L84: hostId=$hostId lxdHost=$lxdHost hostIP=$hostIP kafkaContName=$kafkaContName"    
    echo "server.$hostId=$hostIP:2888:3888"   >> /etc/zookeeper/conf/zookeeper.properties
done


# Restart the server 
/home/kafka/kafka/bin/zookeeper-server-start.sh ~/home/kafka/kafka/config/zookeeper.properties
/home/kafka/kafka/bin/kafka-server-start.sh  /home/kafka/kafka/config/server.properties

