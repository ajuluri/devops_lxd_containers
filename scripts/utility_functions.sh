#!/bin/bash
# Utility script functions used across many scripts. 
# See utility_functions_test.sh for example use


removeDuplicateSlash() {
   echo $(echo $1 | sed -E 's/\/+/\//g')
 }
 
removeDuplicateWhiteSpace() {
  echo $(echo $1 | sed -E 's/\s+/ /g')
}
 

# Return just the file name from a longer path that also
# contains a larger path. 
fileNameFromPath() {
  echo $(basename "$1")
}


# Remove /./ and dir/.. sequences from a pathname and return 
# result 
function normalizePath()
{
    # Remove all /./ sequences.
    local   path=${1//\/.\//\/}
    
    # Remove dir/.. sequences.
    while [[ $path =~ ([^/][^/]*/\.\./) ]]
    do
        path=${path/${BASH_REMATCH[0]}/}
    done
    echo $path
}

function timeMS() {
 echo $(date +%s%N | cut -b1-13)
}