 #!/bin/bash
 # scripts/get_number_of_cores.sh
 # Return the number of cores availble to current machine.
 # NOTE:  Not tested in a container when CPU limited. 
 echo $(cat /proc/cpuinfo | awk '/^processor/{print $3}' | sort | tail -n1)
