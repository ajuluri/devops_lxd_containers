#!/bin/bash
# Return a list of currently available access points
# Requires sudo apt  install network-manager 
echo $(nmcli device wifi list)