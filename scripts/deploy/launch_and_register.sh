#!/bin/bash
#scripts/deploy/launch_and_register.sh
# Launch a named image and map it's one primary listening port
# on host through to a specified port on the client.

templateName=$1
contName=$2
portMap=$3
registerFile=$4
initScript=$5
scriptName="$0"
# Directory where this script exists
scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
scriptBase="$scdir/.."
exampleBase="$scriptBase/../example"
echo 'scdir: $scdir scriptBase: $scriptBase'
source $scriptBase/utility_functions.sh
echo "templateName: $templateName  contName: $contName portMap: $portMap routePrefix: $routePrefix  registerFile: $registerFile initScript=$initScript"

if [ "$#" -lt "5" ]; then
  msg='ABORT: launch_and_register.sh  invalid number of aurguments
  
  $1 - Name of container image used as template to launch
  
  $2 - Name of container to create
  
  $3 - Externally exposed port to map to the container
       to allow processes outside container to connect
       comma delimited string that lists ports to portMap
       example ssh:8008:22,api:9080=80,admin:9100=82          where each mapping is parsed as:
         [1] - prefix used to map in that port in
               layer 7 load balancer.  When set to
               special string NONE then no layer 7
               routing will be done but will still 
               be exposed via layer 4 routing. 
         [2] - host port traffic will be routed to
               from layer 7 load balancer. No attempt 
               is made to determine if the port is in use.
         [3] - Port in container the traffic will 
               be routed to.        

  $4 - File to record the mappings to via appending. Used by other
       parts of system to determine if a given port is already in use.        
       
  $5 - Script to run on host after the container has been started
       This is a script that exists on this host.  Optional
       
  returns 0 response code if sucess otherwise 1
       
  Example: 
    bash launch_and_register.sh  apacheKVkvsample   apacheKVConfig01 "NONE:8200=22,kv:8100=80" mapped.ip.txt noscript    
  '
  # Send msg to stderr
  1>&2 echo "$msg"
  exit 1
fi

# NOTES: 
#  Show list of devices configured for a container
#  lxc config device list apacheKVConfig01
# 
#  Show the actual mapping for a given device on a given 
#  container.
#  lxc config device list apacheKVConfig01 apacheKVConfig01_8100_80
#
#  Check to see if the mapping worked
#  http://192.168.0.201:8001
#

lxc config device show apacheKVConfig01


# TODO: Add option to run a script on the container
#
# TODO: Add check to see if port is in use by a different
#   image name.  If so then abort.


if (lxc image list $templateName | grep " $templateName .*|"); then
  echo "image $templateName exists"
else
  echo "$templateName does not exist but it may suceed if remotely available"
fi

# Remove container if one is already running under that name
bash $scriptBase/container/remove_container.sh $contName

# Launch the container 
lxc launch $templateName $contName
launchOK=$?
echo "launch result: ..$launchOK.."
if [ "$launchOK" = "1" ];  then
  echo "ABORT: launch of $contName from $templateName failed"
  exit 1
fi


bash $scriptBase/container/wait_until_container_has_ip.sh $contName
bexit=$?
echo "bexit=$bexit"
if [ "$bexit" = "99" ]; then
  echo "$contName timed out without an IP exit abort"
  exit 1
fi

containerIP=$(bash $scriptBase/container/get_container_ipv4.sh $contName)
hostIP=$(bash $scriptBase/get_host_ip.sh)
echo "$contName containerIP: $containerIP hostIP: $hostIP"

hportDelim=':'
# Split our Portmap array on ,
IFS=',' read -r -a array <<< "$portMap"
# Process our Input port map and register the
# requested ports to the new container
for spmap in "${array[@]}"
do  
    #Separate route prefix and port assignment
    #into 2 segments to allow sub parsing of 
    #ports.
    l7pref=$(echo $spmap | cut -d ":" -f 1)
    ports=$(echo $spmap | cut -d ":" -f 2)
    hostPort=$(echo $ports | cut -d "=" -f 1)
    contPort=$(echo $ports | cut -d "=" -f 2)
    logStr="contName=$contName,routePrefix=$l7pref,hostIP=$hostIP,hostPort=$hostPort,contIP=$containerIP,contPort=$contPort"
    echo "logStr: $logStr"
    echo $logStr >> $registerFile
    # Call the actual port map function
    bash $scriptBase/container/forward_port_host_to_container.sh $contName $hostPort $contPort    
    mapOk=$?
    echo "mapOk: ..$mapOk.."
    if [ "$mapOk" = "1" ];  then
       echo "ABORT: mapOk:$mapOk  mappingOf: $logStr"
       exit 1
    fi
done
exit 1
