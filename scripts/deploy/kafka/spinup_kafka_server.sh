#!/bin/bash
# spinup_kafka_server.sh
#WIP - UNDER CONSTRUCTION

Cmt="
  $0 
  Spinup a a single Kafka server containter.
  Set enviornment variables specific to that
  Instance.  Hook up Kafka to the right persistent
  storage so we can remount it to a different container
  

  [1] - Template to Launch 
 
  [2] - contName to Launch
    
  [3] - lxdHost - Hostname known to LXD to launch this instance on
  
  [4] - ENV - Enviorment variable to set for current ENV
  
  [5] - kafkaHostId  - Unique Numeric ID for this host instance
  
  [6] - kafkaHostIP - IP or Hostname that will resolve with ping for
        this Kafka Instance
  
  [7] - kafkaHostList - Comma delimited list of hosts or IP in this cluster
        used to modify the Kafka cluster configuration to tell zookeeper
	   and kafka about other servers in the cluster. 
	
  [8] - KV - Keyvalue server with other config paramters.
    
  [9] - Log Destination
  
 
  Example: 
    bash -x $0 basicKafka kafka29 brix1 TEST 29 192.168.1.106 kafkaHosts.txt http://kv.abc.com  logs.abc.com
  
"

XXX="
 
Ports: 
  Default Kafka listens on Port 9092, 
  Default Zookeeper listens on Port 
    2181 - Clients, 
    2888 - follower other zookeeper
    3888 - Internode 
  
 Assumptions:
    One Kafka server will run on a single host. This is intended to
    avoid Conflicts for ports and data locations.
    
    We will start one Kafka server and one zoo keeper server for each 
    server in host list.
    
    We will use the same data directory on each host as the mapped 
    drive.
  
    Current Assumption is that we will use syslog for Kafka to allow
    log aggregation.   
  
    Data will be mapped to /home/kafka/data on the host.  
      That directory must exist on the host and the Kafka container must have write
      privledges on that servers.  

    NOTE: This is a sample of the low level launch utility for a perisisten 
     host cache.  I think this can be abstrated so it becomes nearly identical
     for other non-stateful hosts.  Doing this version to illustrate the concept
   
  See Also:  https://www.cyberciti.biz/faq/run-commands-on-linux-instance-at-launch-using-cloud-init/
   
"


scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
scriptBase="$scdir/../.."

template=$1
contName=$2
lxdHost = $3
ENV=$4
kafkaHostId=$5
kafkaHostIP=$6
kafkaHostList=$7
KV=$8
logDest=$9

imgName=$contName
# launched container should always contain the enviornment
# string to avoid collisions of same named containers from 
# different environments in the same container. 
launchStr=$env$contName

echo "ENV=$ENV  contName=$contName  template=$template lxdHost=$lxdHost kafkaHostId: $kafkaHostId   kafkaHostName: $kafkaHostName   kafkaHostList:  $kafkaHostList  kv: $KV  logDest: $logDest"

if [ "$#" -ne "9" ] ; then
  echo "Invalid number of parameters"
  echo -e "$Cmt"
  exit 1
fi

# If we are not launching on local then Add the hostname to the
# launch string. 
if [ "$lxdHost" = "local" ] ; then
  launchStr="$lxdHost:$env$contName"
  echo "Invalid number of parameters"
  echo $Cmt
  exit 1
fi

echo "ENV=$ENV  contName=$contName  template=$template lxdHost=$lxdHost kafkaHostId: $kafkaHostId   kafkaHostName: $kafkaHostName   kafkaHostList:  $kafkaHostList  kv: $KV  logDest: $logDest launchStr=$launchStr"



# Launch the image and forward the Ports to Host interaface
# Ports mapped here are the default interaction ports for
# the host.
bash $scdir/../launch_and_register.sh  $template   $contName   "NONE:2181=2181,NONE:2888=2888,NONE:3888=3888" mapped.ip.txt noscript
#TODO: Add Error Check from Launch & Register

# Set the Enviornment variables the Instance needs to know about
# to allow Kafka to run in a local Cluster
lxc config set $imgName environment.ENV "$ENV"
lxc config set $imgName environment.kafkaHostId "$kafkaHostId"
lxc config set $imgName environment.kafkaHostIp "$kafkaHostIp"
lxc config set $imgName enviornment.kafkaHostList "$kafkaHostList"
lxc config set $imgName environment.kv "$KV"
lxc config set $imgName environment.logDest "$logDest"
lxc config set $imgName environment.kafkaHostIP "$kafkaHostIp"

# Mount the local storage for the Kafka Instance
# this is needed because Kafka needs data to persist beyond the launch life because
# we want to be able to kill a container, launch a new one and have use the 
# messages already in Queue
lxc config device add $launchStr sdb disk source=/home/kafka path=/home/kafka
lxc config device add $launchStr sdc disk source=/home/zookeeper  path=/home/kafka/zookeeper


# Runs the on-launch to finish local setup based on the current
# configuration parameters. 
lxc exec $launchStr -- bash /home/kafka/on-launch.sh
# TODO:   Figure Out the SSL configuration



#TODO:  Setup of the write storage location on the host.  We may need to use cloud init
#  on a openstack host intended to hold kafka data.    For now we will just create the 
#  directories on the host as /home/kafka/data and /home/kafka/zookeeper and set
#  them to chmod 777.    We will need to write a script that has a open 777 directory where
#  a image can write a request for access.  The script would then pull the UID, PID from 
# that and chmod the /home/kafka/data directory to match.  Or we may need SSH access
# to host to allow the LXD controller to create the directory and set the write privledges.
# See: ssh root@MachineB 'bash -s' < local_script.sh

  
#TODO:  Hookup the log to syslog to remote server.
#TODO: Hookup the log to syslog to remote server via SSH forwarding
# 
