#!/bin/bash

# kafka-host-prep.sh
# Prepares the Kafka Host to be ready to host the Kafka Container.

# Create Directories for Zookeeper and Kafka and allow them write Access.
# We need this so the data in these directories is preserved past the life 
# of the container when we launch a version of the Kafka container.
sudo mkdir -p /home/kafka/data
sudo mkdir -p /home/kafka/logs
sudo mkdir -p /home/zookeeper/data
sudo mkdir -p /home/zookeeper/logs

sudo chmod -R 777 /home/kafka
sudo chmod -R 777 /home/zookeeper
 
 