#!/bin/bash
# Demonstrate using the specific KV zip input
# to produce an apache that can be used for
# as a key/value config store.
#
# Also demonstrates extending a generic Apache 
#  image to serve as a KV and then further
#  extending that to load with a specific KV
#  set. 

scdir=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cwd=$(pwd)
fiName=$scdir/kv
echo "cwd: $cwd  scdir=$scdir fiName=$fiName"
 
bash $scdir/../../scripts/image/produce/apacheKV.sh kvsample $fiName
#   parm 1 = suffix to add to the apacheKV imageName to make 
#     it easier to differentiate differnt config store images
