with open('./server.xml', 'r') as myfile:
    data=myfile.read().replace('\n', '')

start = "Connector port=\"8080\""
repl = "Connector port=\"8083\""
newStr = data.replace(start,repl)

with open('./server.xom', 'w') as myfile:
   myfile.write(newStr)