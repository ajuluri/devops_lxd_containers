# Basic LXD Container commands

###### First Time through lxd

* See Also [basic_ubuntu_18_host_setup.sh](../scripts/host_prep/basic_ubuntu_18_host_setup.sh) for a script that will do most of the basic setup for you.   One thing you will need to know before hand is the block device you want LXD to use for storage unless you plan to use the dir option during initialization.

```sh
#Update local packages
sudo apt update
sudo apt-get dist-upgrade -y
```

```sh
# Utilities we commonly use.
sudo apt-get install acl xzip tar rsync pxz xz-utils python-lzma cgroup-bin libpam-systemd unzip zip libarchive-tools python golang p7zip-full zsh jq -y
```

```SH
#install lxc to get all commands
#otherwise lxc-attach is missing
#and is needed for some configuration commands.
sudo apt-get install lxc -y
```

```sh
#Install lxd
sudo apt install lxd -y
# I choose the option for 3.0 first and then 
# removed it and chose lattest features. 
```

```sh
#Add your user to lxd group
sudo adduser USERNAME lxd
```



```sh
# If you get a socket error when trying 
# to run lxc commands without socket
# it worked when I removed it and reinstalled.

systemctl kill snap.lxd.daemon
sudo pkill lxd # or any other way to kill all those dangling lxd processes if any is left
systemctl start snap.lxd.daemon
systemctl status snap.lxd.daemon

# Trye the following if the first did not work
sudo apt purge lxd -y
sudo apt install lxd -y
```

```sh
# Install some of our commonly used tools

# install python 2.7
sudo apt install python -y
# install go language
sudo apt install golang -y


```

```
# clone the devops lxd containers project 
# to local drive into the devops directory.

git clone https://bitbucket.org/joexdobs/devops_lxd_containers devops

git clone https://bitbucket.org/joexdobs/computer-aided-call-response-engine cacre

git clone https://bitbucket.org/joexdobs/meta-data-server mds 

```



###### lxd is already working

```sh
#Get a list of running containers
#Also shows the IP address for every container
lxc list
```

```sh
# List set of images available in default
lxc image list
```

```sh
# Shows only ubuntu stable images
lxc image list ubuntu: | less
```

```sh
# Adding Minimal Ubuntu Images
# Minimal images boot in far less RAM so you can run more of them
# on a given Host. It also takes less time to boot compared to the 
# full image.
lxc remote add --protocol simplestreams ubuntu-minimal https://cloud-images.ubuntu.com/minimal/releases/

# Display the minimal Images
lxc image list ubuntu-minimal:

# Launch a Minimal Ubuntu Image
lxc launch ubuntu-minimal:18.10

```

```sh
# Boot a containter running Ubuntu 18.04
# Given a name of first.
lxc launch ubuntu:18.04 first
#   If success returns 0 in $?
#   if fail returns 1 in $?
```

```sh
# Shows this list of running images
lxc list
```

```sh
# Show basic information including network config
# for image named first.
lxc info first

# Shows additional information including image version
# for named image.
lxc config show first
```

```sh
# Show the Container logs
lxc info mycontainer --show-log
```

```sh
# Show Free memory at host level
free -m

# Show Free Memory for a container running in host
lxc exec first -- free -m
```

```sh
# Limit memory usage for container named first to 128 Meg
lxc config set first limits.memory 128MB
```

```sh
# Using AptGet to control contents in a container
# also can be used to install new applications
lxc exec first -- apt-get update
lxc exec first -- apt-get dist-upgrade -y
lxc exec first -- apt-get autoremove --purge -y

```

```sh
# Create a reusable snapshot of our image called clean
lxc snapshot first clean
```

```sh
# Run a arbitrary linux command in the container named
# first.  This one will essentially destroy the container
# so take a snapshot first.
lxc exec first -- rm -Rf /etc /usr
```

```
# Run arbitary linux command in container named first
# This one delivers a bash shell. 
lxc exec first -- bash
```

```sh
# Restore a Snapshot
lxc restore first clean
```

```sh
# Stop a running Image
lxc stop first

# Delete a Image so it can not be restored
lxc delete first
```





```sh
# Run arbitrary command in container. This 
# one delivers a bash shell we can directly
# interact with
lxc exec first -- bash
```

```sh
# Publish an Alias of a saved Image with a 
# user friendly name clean-ubuntu
lxc publish first/clean --alias clean-ubuntu

```

```sh
# Launch a previously published Ubuntu Image
# with a new name "second"
lxc launch clean-ubuntu second
```

```
#..
```

### Set Environment variable for a container while running

```
lxc config set x1 environment.test joe
  #  x1 is image name
  #  enviornment is lxd key constant indicating you want to 
  #  set a enviornment variable
  # .test is the specific enviornment variable we are setting
  # joe is the value we are setting
  # NOTE: See the space between the key and value rather than
  #  = like we normally do.

# Now see if it is still set
lxc exec x1 -- env | grep test


# Another sample set the ENV environment varible to TST
lxc config set x1 environment.ENV TST

# See if it is still set
lxc exec x1 -- env | grep ENV

# Another sample set the Key Value server URL where to go to 
# fetch configuration values
lxc config set x1 environment.KVServer https://kv-config.ABC.COM

# See if it is still set
lxc exec x1 -- env | grep KVServer

# Another Sample pass in the list of servers where we have Kafka
# installed.
lxc config set x1 environment.KafkaServers,kafka1.abc.com, kafka2.abc.com,kafka3.abc.com

# See if it is still set
lxc exec x1 -- env | grep KafkaServers




```

* Variables set with the lxc config are set only for the life of the current container.  They are not persisted as part of the container when snapshotted.  As such you need to either reset them when the image is launched as a new container or you need to save the variables inside the container. 
* 

```
#..
```





```
#..
```



```
#..
```



```
#..
```



```
#..
```



```
#..
```



```
#..
```



```
#..
```



```sh
TODO:  Need to Test this and provide Better Overview
#https://www.cyberciti.biz/faq/how-to-auto-start-lxd-containers-at-boot-time-in-linux/

#Auto Start when VM Starts
lxc config set nginx-vm boot.autostart true
 
#Delay before starting next one
lxc config set nginx-vm boot.autostart.delay 10

# Controll Ordering of LXD Boot
$ lxc config set db_vm boot.autostart.priority 100
$ lxc config set nginx_vm boot.autostart.priority 99

# How to run commands on linux container at LXD provision or launch
# https://www.cyberciti.biz/faq/run-commands-on-linux-instance-at-launch-using-cloud-init/
# See LXD clout-init
# Apply profiles
# Install SSL Keys 

# Pass Cloud init directives to instance with user data
$ lxc config set foo user.user-data - < config.yml

# To view your lxc config for foo container, run:
$ lxc config show foo

#Run command on first boot only

# Configure to load the SSH at boot time.
bootcmd:
 - [sh, -c, "echo 'ListenAddress 192.168.1.100' >> /etc/ssh/sshd_config"]
 - systemctl reload ssh
 
# Set the Hostname
hostname: foo
fqdn: foo.nixcraft.com
manage_etc_hosts: true

# Upgrade Pakcages at Boot time
# Apply updates using apt
package_upgrade: true

# run command First Boot Only
#Run command on first boot only
bootcmd:
 - [sh, -c, "echo 'ListenAddress 192.168.1.100' >> /etc/ssh/sshd_config"]
 - systemctl reload ssh
 
How to Install LXD


```

* [How to install LXD Container under KVM or Xen Virtual machine](https://www.cyberciti.biz/faq/how-to-install-lxd-container-under-kvm-or-xen-virtual-machine/)

```
# Launch a Oracle Linux Container 
lxc launch images:oracle/7/amd64 oracle7-c9
```

### Allow LXC container to access a host Directories and Files

###### Grant LXC container write privileges on host 

```sh
# Allow LXC container to access a host device 

# Create the Directory we want to allow access to
mkdir -p ~/test

# We want to create another directory where we can 
# allow the container to write that will allow us
# to harvest the user and group it is using. 
# It has to be created before we mount the device
mkdir -p ~/test/getuid
chmod -R 777 ~/test/getuid

# Create a small file to test access
echo -e "joe\njime\njack\jimbo\taby" > ~/test/file.txt

ps -eo pid,comm,euser,supgrp,user,group,cmd | grep x1

# Setup the ownership for hte container PID to give write privleges
chown -R  1000000:1000000 ~/test

# Map the mountable directory into the container
lxc config device add x1 sdb disk source=~/test path=mnt/test
#   lxc config device add -  constant used in comand
#   x1 - name of the image
#   sdb - name of device you are creating
#   source - path host directory you are exposing
#   path - path in container to expose path as


# Test that we can access the file
lxc exec x1 -- cat /mnt/test/file1.txt
 
 
# Now lets try to write to the file.
# This part is a little harder because we either need
# to allow RW for everyone or we need to figure out
# what PID, UID LXD will use when this containter 
# tries to write.    We cheated up above by creating
# a wide open directory ~/test/guid so we can save 
# a file and see what the OS records as the user and
# group Id. We can use that to go back and change the
# ownership 

#  Copy a file inside the container should reset 
# permissions to container uid, gid but we are writting
# it to a shared directory we can read from the host
lxc exec x1 -- cp /mnt/test/file1.txt /mnt/test/getuid/file1.txt

ls -l ~/test/getuid/file1.txt
uid=$(ls -l ~/test/getuid/file1.txt | cut -d " " -f 3)
gid=$(ls -l ~/test/getuid/file1.txt | cut -d " " -f 4)
echo "uid=$uid  gid=$gid"

# Now we can change the actual data directory we care about
# with the correct permissions to allow the container to 
# read and write that directory 
sudo chown -R $uid:$gid ~/test

# Test it to see if we can write a file to the main mount now
lxc exec x1 -- cp /mnt/test/getuid/file1.txt /mnt/test/xxx.txt 
 
# Remove / Unmount the Device
lxc config device remove x1 sdb



```



```
#..
```

## Working with Remote LXD Hosts

### Adding a remote  LXD host 

```sh
lxc remote add brix1 192.168.1.102 --password aPassword
# This assumes that on the remote host that you chose yes to expose the LXD
# and that you selected the default port 8443 and that you selected
# a first time connection password that you still remmber.
#
# brix1 is the canonical name we can use in our commands
# 192.168.1.102 is the IP address of the remote host
# aPassword is the trust password you assigned when you initialized LXD
#  on the remote host. 
#
# Once you run the command it will ask if it is OK to accept the cert from 
# the remote host.  There does not seem to be any way to automatically accept 
# the cert without human intervention. 

```

It is important to remember that brix1 is a canonical name for lxd.  It has nothing to do with DNS or local hosts.  It is assigned during the remote add operation. 

* [Adding a remote with a PKI based setup](https://lxd.readthedocs.io/en/latest/security/)  We may need this because the current host add requires breaking the automated flow when adding servers to the cluster.  See aAlso: Adding a remote with Candid
* 
* 

### List running containers on a [remote  LXD host](https://stgraber.org/2016/04/12/lxd-2-0-remote-hosts-and-container-migration-612/)

```sh
lxc list brix2:

# brix2 is the name of the remote host you previously added with 
#   the remote add. 
```

### List  LXD Images on Remote Host

```sh
 lxc image list brix2:
 
 # brix2 is the name of the remote host you previously added with the 
 # remote add.
```

### Start a  LXD container on remote host using a local Image

```sh
lxc launch basicKafka brix2:kftst

# basicKafka - is a template images that has been created on the local LXD
#   Host.  It will automatically be pushed to the remote host.
#
# brix2: is the name of a host previously added wiht the remote add
#   command.
#
# kfktst is the name you want the running container to have on the 
#  remote host. 
# 

```

### Start a  LXD  container on remote host using a remote Image

```sh
lxc launch brix2:basicTomcat brix2:tmct1

 # brix2:basicTomcat is the image to launch the first part brix2 is the 
 # machine name the image resides on.  The second part basicTomcat is the
 # name of a template image previously created on that server. 
 
```

### Show configuration of a  LXD container running on a remote host

```sh
# Show configuration of a container running on a remote host
lxc config show brix2:tmct1
```

### Set memory limitation for  LXD container running on remote host

```sh
# Set memory limitation for container running on remote host
lxc config set brix2:tmct1 limits.memory 128MB
```

### Show available memory for a  LXD container running on remote host

```sh
# Show available memory for a container running on remote host
lxc exec brix2:tmct1 -- free -m
```

### Take a snapshot of a remote  LXD container and then restore it

```
# Take a snapshot of a remote container and then restore it to
# it's previous state.  Note unlike published images we can do this while a container
# is running and it is quite a lot faster than saving a template and then re-launching
# it as a new container. 
lxc snapshot brix2:tmct1 clean
lxc restore brix2:tmct1 clean
```

###  Pull a file from a remote  LXD container

```
lxc file pull brix2:tmct1/etc/hosts .
```

### push a file to a remote  LXD container

```
lxc file push hosts brix2:tmct1/etc/hosts
```

### Pull  log files from remote LXD container

```
lxc file pull brix2:tmct1/var/log/syslog - | less
```

#### Stop and delete a remote  LXD  container

```
lxc delete --force brix2:tmct1
```

#### List all configured Remote LXD Hosts

```
lxc remote list
```

Add Trust Cert directly

```
# https://lxd.readthedocs.io/en/latest/
lxc config set core.trust_password SECRET
lxc config trust add client.crt
# 
```



```

https://blog.ubuntu.com/2018/01/26/lxd-5-easy-pieces

```

### Copy Image from remote host to local host

```
lxc image copy <remote name>:/path/to/image local: --alias=<image name>
```



```
#..
```





------



# Getting LXD working in Ubuntu running in Virtual Box

I used the option during install to install ubuntu 18.04 to install lxd and docker during the install process this worked but still took a few steps extra.

```sh
lxd init
# When it askes about backend use dir and take the rest as defaults.

Results of lxd init
Would you like to use LXD clustering? (yes/no) [default=no]:
Do you want to configure a new storage pool? (yes/no) [default=yes]:
Name of the new storage pool [default=default]: defaultx
Name of the storage backend to use (btrfs, dir, lvm) [default=btrfs]: dir
Would you like to connect to a MAAS server? (yes/no) [default=no]:
Would you like to create a new local network bridge? (yes/no) [default=yes]:
What should the new bridge be called? [default=lxdbr0]: lxbr1
What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]:
What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]:
Would you like LXD to be available over the network? (yes/no) [default=no]: yes
Address to bind LXD to (not including port) [default=all]:
Port to bind LXD to [default=8443]:
Trust password for new clients:
Again:
Would you like stale cached images to be updated automatically? (yes/no) [default=yes]
Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]:



```

###### Yaml from lxd init

```
config:
  core.https_address: '[::]:8444'
  core.trust_password: apple99X
networks:

- config:
  ipv4.address: auto
  ipv6.address: auto
    description: ""
    managed: false
    name: lxdbr1
    type: ""
  storage_pools:
- config: {}
  description: ""
  name: default
  driver: dir
  profiles:
- config: {}
  description: ""
  devices:
    eth0:
      name: eth0
      nictype: bridged
      parent: lxdbr1
      type: nic
    root:
      path: /
      pool: default
      type: disk
  name: default
  cluster: null
```



# Installing Ubuntu using pxe onto bare servers

* [Setting up a 'PXE Network Boot Server' for Multiple Linux Distribution Installations in RHEL/CentOS 7](https://www.tecmint.com/install-pxe-network-boot-server-in-centos-7/)
* 

