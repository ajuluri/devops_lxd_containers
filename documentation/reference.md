# Reference & Research 

* [The biggest difference between openstack and kubernetes is timing](https://architecht.io/the-biggest-difference-between-openstack-and-kubernetes-is-timing-720d31ef5035)
* [How we Architected and Ran Kubernetes on Openstack at scale at Yahoo!  Japan](https://kubernetes.io/blog/2016/10/kubernetes-and-openstack-at-yahoo-japan/)
* [The case for LXD, Canonicals System Container framework](https://containerjournal.com/2017/06/02/case-lxd-canonicals-system-container-framework/)
* 

# LXD Lightweight containers

* See Reference section in [devops with lxd](devops-with-lxd.md#Reference & Links)
* [My exploration with LXD containers as basis for Devops work](devops-with-lxd.md)


# Installing Open stack 

* [Install Openstack on Ubuntu with Conjure-up](https://www.ubuntu.com/openstack/install#workstation-deployment)

* [OpenStack Docs: Stop and start an instance](https://docs.openstack.org/newton/user-guide/cli-stop-and-start-an-instance.html)

* [OpenStack Docs: OpenStack command-line interface cheat sheet](https://docs.openstack.org/mitaka/user-guide/cli_cheat_sheet.html)

* [Canonical Bootstack whitepaper](https://pages.canonical.com/Softlayer-BootStack.html)

* [Wiki Openstack building the cluster](https://wiki.openstack.org/wiki/Documentation/training-labs#Building_the_cluster)

* [Cannonical Kubernetes Bootstack](https://www.ubuntu.com/kubernetes)  They manage the under cloud so you can focus on the actual app.

  ### Running Ubuntu on Windows Home

* [Install openstack and k8 on windows home](install-openstack-and-k8-on-windows-7-virtual-box-ubuntu.md)





  ### Open stack on Mac

* [Openstack Traininglabs Binary to run Openstack in VirtualBox](https://docs.openstack.org/training_labs/)

* [Mirantis - installation of Openstack using VirtualBox](https://www.mirantis.com/how-to-install-openstack/)

* [Intalling Openstack from a Working Python on Mac](https://docs.openstack.org/nova/queens/contributor/development-environment.html)

* [Attempt to install Openstack using Vagrant + Puppet](https://github.com/openstack-dev/devstack-vagrant/blob/master/README.md)

# Kubernetes on Ubuntu

* [Kubernetes on Ubuntu with conjur-up](https://kubernetes.io/docs/getting-started-guides/ubuntu/)
* [Creating and Accessing a Kubernetes Cluster on Openstack #1 Create the Cluster](https://www.mirantis.com/blog/creating-accessing-kubernetes-cluster-openstack-part-1-create-cluster/)
* [How to run Kubernetes cluster in Openstack](http://superuser.openstack.org/articles/how-to-run-a-kubernetes-cluster-in-openstack/)  Uses the Magnum API



## Kubernetes on Openstack

* [Deploy Kubernetes on Openstack with mostly command line commands](https://gbraad.gitbooks.io/kubernetes-handsonlabs/content/deployment/)
* 



# GO Knowledge

* [Preparing a GO Package for import](https://github.com/golang/go/wiki/PackagePublishing)
* [Supporting Unit Tests in GO](https://blog.alexellis.io/golang-writing-unit-tests/)