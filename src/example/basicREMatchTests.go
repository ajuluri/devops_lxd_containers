/* Simple framework to test RE Pattern matches.

See Also:
  https://gobyexample.com/regular-expressions
  https://www.rexegg.com/regex-quickstart.html
  http://www.cbs.dtu.dk/courses/27610/regular-expressions-cheat-sheet-v2.pdf
  https://medium.com/factory-mind/regex-tutorial-a-simple-cheatsheet-by-examples-649dc1c3f285

*/

package main

import (
	"fmt"
	"regexp"
)

// Test basic matching show where it matches or that
// it does not match.
func basicMatchTest(rePat string, source []byte) string {
	var slen = len(source)
	pattern := regexp.MustCompile(rePat)
	fmt.Println("basicMatchTest rePatt=", rePat, "source length=", slen)
	loc := pattern.FindIndex(source)
	if len(loc) < 2 {
		fmt.Println("basicMatchTest No Match on pattern=", rePat)
		return ""
	}
	fmt.Println("basicMatchTest first match Loc=", loc, "Match String=", string(source[loc[0]:loc[1]]))
	return string(source[loc[0]:loc[1]])
}

func main() {

	var contentLXCResp = []byte(`
Name: tomcat
Location: none
Remote: unix://
Architecture: x86_64
Pid: 10742
Ips:
  eth0: inet    10.41.16.64     veth1L725X
  eth0: inet6   fd42:a09:55ba:c16a:216:3eff:fea6:a0cb   veth1L725X
  eth0: inet6   fe80::216:3eff:fea6:a0cb        veth1L725X
  lo:   inet    127.0.0.1
  lo:   inet6   ::1
Resources:
`)

	_ = basicMatchTest(`\s?eth\d+:`, contentLXCResp)
	_ = basicMatchTest(`\s?eth\d+:\s*`, contentLXCResp)
	_ = basicMatchTest(`\s?eth\d+:\s*inet\s*`, contentLXCResp)
	_ = basicMatchTest(`\s?eth\d+:\s*\d`, contentLXCResp)
	_ = basicMatchTest(`\s?eth\d+:\s*\d{1}`, contentLXCResp)

	fmt.Println("Start validate is IP Pattern")
	var testIP = []byte(`apple 10.41.16.64 in my eye`)
	_ = basicMatchTest(`\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}`, testIP)

}
